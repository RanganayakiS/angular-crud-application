﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E4_UserPortal.Models
{
   
    public class E4_Users
    {
        [Key]
        public int? UserId { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
        public Int64 telephone_Number { get; set; }
        public string city { get; set; }
        public Int64 dateofBirth { get; set; }
        public string Notes { get; set; }
        
    }
}
