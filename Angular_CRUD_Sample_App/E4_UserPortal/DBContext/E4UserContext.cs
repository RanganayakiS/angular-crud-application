﻿using E4_UserPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E4_UserPortal.DBContext
{
    public class E4UserContext
    {
        public List<E4_Users> UserList = new List<E4_Users>();

        public E4UserContext()
        {
            UserList.Add(new E4_Users
            {
                UserId = 1,
                firstName = "John",
                surname = "Stewart",
                gender = "Male",
                email = "jstewatt@e4.co.za",
                telephone_Number = 9849853963,
                city = "Johannesburg",
                dateofBirth = 290386,
                Notes = "This is test Notes",


            });


            UserList.Add(new E4_Users
            {
                UserId = 2,
                firstName = "Amanda",
                surname = "Daniels",
                gender = "Male",
                email = "AmandaD@e4.co.za",
                telephone_Number = 9839853963,
                city = "Johannesburg",
                dateofBirth = 290386,
                Notes = "This is test Notes2",


            });


            UserList.Add(new E4_Users
            {
                UserId = 3,
                firstName = "Jenny",
                surname = "Singh",
                gender = "Male",
                email = "SJenny@e4.co.za",
                telephone_Number = 9849453963,
                city = "Johannesburg",
                dateofBirth = 291386,
                Notes = "This is test Notes3",
            });
        }
    }
}
