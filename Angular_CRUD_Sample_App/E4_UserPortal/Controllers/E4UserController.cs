using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using E4_UserPortal.DBContext;
using E4_UserPortal.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace E4_UserPortal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class E4UserController : ControllerBase
    {
        readonly E4UserContext EmpDetails;

        public E4UserController()
        {
            EmpDetails = new E4UserContext();
        }

        [HttpGet]
        public IEnumerable<E4_Users> Get()
        {
            var data = EmpDetails.UserList;
            return data;
        }

        [HttpPost]
        public IActionResult Post([FromBody] E4_Users obj)
        {
            EmpDetails.UserList.Add(obj);
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] E4_Users obj)
        {
            var user = EmpDetails.UserList.Select(x => x.UserId == id) as E4_Users;
            EmpDetails.UserList.Remove(user);
            EmpDetails.UserList.Add(obj);
            return Ok();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = EmpDetails.UserList.Select(x => x.UserId == id) as E4_Users;
            EmpDetails.UserList.Remove(user);
            return Ok();

        }
    }
}