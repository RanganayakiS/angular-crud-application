import { Component, Inject, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalComponent } from '../modal/modal.component';
import { ModalConfig } from "../modal/modal.config";
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { NgbAlert, NgbAlertConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchPipe } from '../search.pipe';
import { User } from 'oidc-client';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { UserService } from '../services/UserService';
import {
  MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule
} from "@angular/material";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  public elements: any = [];
  public users: Users[];
  public searchTerm: "";
  public id: string;
  editField: string;
  private contentEditable: boolean = false;
  page = 1;
  pageSize = 7;
  public collectionSize: number;
  http: HttpClient
  baseUrl: string
  staticAlertClosed = false;
  private _success = new Subject<string>();
  public alertType: string
  public alertTypePrefix: string
  public successMessage: string
  public alerts: any = [];
  
  @ViewChild('addUserModal', { static: false }) private addUserModal: ModalComponent
  @ViewChild('editUserModal', { static: false }) public editUserModal: ModalComponent
  @ViewChild('viewUserModal', { static: false }) private viewUserModal: ModalComponent
  @ViewChild('staticAlert', { static: false }) private staticAlert: NgbAlert;
  @ViewChild('selfClosingAlert', { static: false }) private selfClosingAlert: NgbAlert;

  public modalConfig: ModalConfig = {
    onDismiss: () => {
      return true
    },
    hideDismissButton: () => { return true },
    onClose: () => {
      return true
    },
    closeButtonLabel: "Cancel"
  }
  

  constructor(private userService: UserService, private confirmationDialogService: ConfirmationDialogService, private modalService: NgbModal) {
    this.loadUsers()
  }

  loadUsers() {
    this.userService.getAll().subscribe(result => {
      this.collectionSize = result.length;
      this.users = result;
    }, error => this.errorNotification());
  }

  async onAddUser() {
    
    this.modalConfig.modalTitle = "Add User"
    this.addUserModal.modalConfig.hideDismissButton;
    await this.addUserModal.open()
    this.loadUsers()
  }

  changeValue(id: string, property: string, event: any) {
    
    this.editField = event.target.textContent;
  }
  updateList(id: string, property: string, event: any) {
    var user = this.users.find(x => x.id === id);
    const editField = event.target.textContent;
     user[property] = editField;

    this.userService.update(id, JSON.stringify(user)).subscribe(res => {

      this.alerts = [{
        type: 'success',
        message: 'Updated successfully'
      }];
      this.alertTypePrefix = "Success !"
      setTimeout(() => this.alerts.splice(this.alerts.indexOf(alert), 1), 5000); // Close Alert automatically

    }, error => this.errorNotification());
  }

  errorNotification() {
    this.alerts = [{
      type: 'danger',
      message: 'Server Error!'
    }];
    this.alertTypePrefix = "Error !"
    setTimeout(() => this.alerts.splice(this.alerts.indexOf(alert), 1), 5000); // Close Alert automatically
  }

  search(value: string): void {

    var res = this.users.filter((val) => val.firstName.toLowerCase().includes(value) || val.surname.toLowerCase().includes(value) || val.city.toLowerCase().includes(value) || val.dateOfBirth.toLowerCase().includes(value) || val.email.toLowerCase().includes(value) || val.telephoneNumber.toLowerCase().includes(value) || val.notes.toLowerCase().includes(value));
    this.collectionSize = res.length;
  }

  close(alert: Alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  eventsSubject: Subject<string> = new Subject<string>();

  async onViewUser(user: string) {
    this.modalConfig.modalTitle = "View User Details"
    this.eventsSubject.next(user);
    return await this.viewUserModal.open()
  }

   onClose() {
   this.editUserModal.close();
  }

  setEditableTable(isEnabled: boolean)
  {
    console.log(isEnabled)
    this.contentEditable = isEnabled;
  }

  async onEditUser(user: string) {
    this.modalConfig.modalTitle = "Edit User"
    this.eventsSubject.next(user);
    await this.editUserModal.open()
    this.loadUsers()
  }


  async onDelete(user: Users) {
    this.confirmationDialogService.confirm('Delete user confirmation', 'Do you want to delete this user ?', 'Yes', 'No',"lg")
      .then((confirmed) => {
        if (confirmed) {
          this.userService.delete(user.id).subscribe(res => {
            this.loadUsers()
          })
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
interface Alert {
  type: string;
  message: string;
}


interface Users {
  id: string
  firstName: string
  surname: string
  gender: string
  email: string
  telephoneNumber: string
  city: string
  dateOfBirth: string
  notes: string
}
