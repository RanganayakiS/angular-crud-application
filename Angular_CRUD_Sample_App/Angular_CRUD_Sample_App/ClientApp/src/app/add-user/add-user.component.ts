import { Component, Inject, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router'
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { DatePipe } from '@angular/common'
import { Subscription } from 'rxjs';
import { User } from '../models/User';
import { UserService } from '../Services/UserService';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styles: [
    "../node_modules/bootstrap/dist/css/bootstrap.min.css",
    "./src/styles.css"]
})
export class AddUserComponent {
  @Input() readOnly: Boolean;
  @Input() parentModal: ModalComponent;
  @Input() isEditing: Boolean;
  @Input() isViewing: Boolean;
  @Input() userId: string;

  user: User
  minDate: Date;
  route: Router
  model: NgbDateStruct
  public alertType: string
  public alertTypePrefix: string
  public successMessage: string
  public alerts: any = [];
  constructor(private formBuilder: FormBuilder, public datepipe: DatePipe, private userService: UserService) {
  }

  private eventsSubscription: Subscription;

  @Input() events: Observable<string>;

  ngOnInit() {
    this.eventsSubscription = this.events.subscribe((data) => this.onEdit(data));
  }

  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }

  async onEdit(userId: string) {
    if (this.isEditing || this.isViewing) {
      this.userId = userId
      this.loadUser(userId)
    }
  }

  loadUser(id: string) {
    this.userService.get(id)
      .subscribe(result => {
        this.userForm.controls['firstname'].setValue(result.firstName)
        this.userForm.controls['lastname'].setValue(result.surname)
        this.userForm.controls['email'].setValue(result.email)
        this.userForm.controls['city'].setValue(result.city)
        this.userForm.controls['dateofBirth'].setValue(this.convertStringToDate(result.dateOfBirth))
        this.userForm.controls['gender'].setValue(result.gender.toLocaleLowerCase())
        this.userForm.controls['telephone'].setValue(result.telephoneNumber)
        this.userForm.controls['notes'].setValue(result.notes)

      }, error => console.error(error));
  }

  userForm = this.formBuilder.group({
    firstname: new FormControl(),
    lastname: new FormControl(),
    surname: new FormControl(),
    email: new FormControl(),
    city: new FormControl(),
    dateofBirth: new FormControl(),
    gender: new FormControl(),
    telephone: new FormControl(),
    notes: new FormControl(),
  });

  addUser(user: User): void {
    this.userService.add(JSON.stringify(user)).subscribe(res => {
      this.alerts = [{
        type: 'success',
        message: 'User added successfully'
      }];
      this.alertTypePrefix = "Success !"
      setTimeout(() => this.onClose(), 5000);
      setTimeout(() => this.alerts.splice(this.alerts.indexOf(alert), 1), 5000);
    }, error => this.errorNotification())
  }

  private dateToString = (date) => `${date.year}-${date.month}-${date.day}`;

  convertStringToDate(date: string) {
    var dateParts = date.substring(0, 10).split("-");

    return {
      year: parseInt(dateParts[0]),
      month: parseInt(dateParts[1]),
      day: parseInt(dateParts[2])
    };
  }

  onClose() {
    this.userForm.reset()
    this.parentModal.close()
  }

  onSave(): void {
    if (this.userForm.valid && this.userForm.valid) {
      this.user = new User()
      this.user.firstName = this.userForm.get('firstname').value
      this.user.surname = this.userForm.get('lastname').value
      this.user.email = this.userForm.get('email').value
      this.user.city = this.userForm.get('city').value
      this.user.dateOfBirth = this.dateToString(this.userForm.get('dateofBirth').value);
      this.user.gender = this.userForm.get('gender').value
      this.user.telephoneNumber = this.userForm.get('telephone').value
      this.user.notes = this.userForm.get('notes').value
      this.saveUser(this.userId, this.user)
    }
  }

  saveUser(userId: string, user: User) {
    this.userService.update(userId, JSON.stringify(user)).subscribe(res => {
      this.alerts = [{
        type: 'success',
        message: 'User updated successfully'
      }];
      this.alertTypePrefix = "Success !"
      setTimeout(() => this.onClose(), 5000);
      setTimeout(() => this.alerts.splice(this.alerts.indexOf(alert), 1), 5000);
    }, error => this.errorNotification())
  }

  errorNotification() {
    this.alerts = [{
      type: 'danger',
      message: 'Server Error!'
    }];
    this.alertTypePrefix = "Error !"
    setTimeout(() => this.alerts.splice(this.alerts.indexOf(alert), 1), 5000); // Close Alert automatically
  }

  onSubmit(): void {
    if (this.userForm.valid && this.userForm.valid) {
      this.user = new User()
      this.user.firstName = this.userForm.get('firstname').value
      this.user.surname = this.userForm.get('lastname').value
      this.user.email = this.userForm.get('email').value
      this.user.city = this.userForm.get('city').value
      this.user.dateOfBirth = this.dateToString(this.userForm.get('dateofBirth').value);
      this.user.gender = this.userForm.get('gender').value
      this.user.telephoneNumber = this.userForm.get('telephone').value
      this.user.notes = this.userForm.get('notes').value
      this.addUser(this.user)
    }
  }
}

export class NgbdDatepickerPopup {
  model: NgbDateStruct;
}



