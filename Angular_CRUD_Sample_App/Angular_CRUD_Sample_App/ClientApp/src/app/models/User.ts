export class User {
  firstName: string
  surname: string
  gender: string
  email: string
  telephoneNumber: string
  city: string
  dateOfBirth: string
  notes: string
}
