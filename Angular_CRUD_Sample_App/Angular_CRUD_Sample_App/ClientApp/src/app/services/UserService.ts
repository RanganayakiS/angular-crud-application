import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseURL = 'https://e4pocapi.azurewebsites.net/api/ContactDetails';
const getAllURL = 'https://e4pocapi.azurewebsites.net/api/lstContactDetails';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<any> {
    return this.httpClient.get(getAllURL)
  }

  get(id): Observable<any> {
    return this.httpClient.get(`${baseURL}/${id}`);
  }

  add(data): Observable<any> {
    return this.httpClient.post(baseURL, data, httpOptions);
  }

  update(id, data): Observable<any> {
    return this.httpClient.put(`${baseURL}/${id}`, data, httpOptions);
  }

  delete(id): Observable<any> {
    return this.httpClient.delete(`${baseURL}/${id}`);
  }
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};
