import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'pageFilter'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!args) {
      return value;
    }
    return value.filter((val) => {
      let rVal = (val.firstName.toLocaleLowerCase().includes(args) || (val.notes.toLocaleLowerCase().includes(args)) ||(val.dateOfBirth.toLocaleLowerCase().includes(args))|| (val.city.toLocaleLowerCase().includes(args))|| (val.surname.toLocaleLowerCase().includes(args)) || (val.gender.toLocaleLowerCase().includes(args)) || (val.email.toLocaleLowerCase().includes(args)) || (val.telephoneNumber.includes(args)));

      return rVal;
    })

  }

}
