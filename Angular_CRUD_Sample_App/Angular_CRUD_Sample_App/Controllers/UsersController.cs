﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular_CRUD_Sample_App.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly E4UserContext _context;
        public UsersController(ILogger<UsersController> logger)
        {
            _logger = logger;
            if (_context == null)
            {
                _context = new E4UserContext();
            }
        }

        [HttpGet]
        public IEnumerable<E4_Users> Get()
        {
            return _context.UserList.ToArray();
        }


        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            return Ok();
        }

        [HttpGet("{id:int}")]
        public ActionResult Get(int id)
        {
            var result = _context.UserList.FirstOrDefault(x => x.id == id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<E4_Users>> Post(E4_Users user)
        {
            _context.UserList.Add(user);

            return CreatedAtAction(nameof(E4_Users), new { id = user.id }, user);
        }

        //[HttpGet]
        //public object getAllUsersCount()
        //{

        //    var userDetailsCount = _context.UserList.ToArray().Count();
        //    return userDetailsCount;
        //}
    }

    public class E4UserContext
    {
        public List<E4_Users> UserList = new List<E4_Users>();

        public E4UserContext()
        {
            UserList.Add(new E4_Users
            {
                id = 1,
                firstName = "John",
                surname = "Stewart",
                gender = "Male",
                email = "jstewatt@e4.co.za",
                telephone_Number = "9849853963",
                city = "Johannesburg",
                dateofBirth = "290386",
                notes = "This is test Notes",


            });


            UserList.Add(new E4_Users
            {
                id = 2,
                firstName = "Amanda",
                surname = "Daniels",
                gender = "Male",
                email = "AmandaD@e4.co.za",
                telephone_Number = "9839853963",
                city = "Johannesburg",
                dateofBirth = "290386",
                notes = "This is test Notes2",


            });


            UserList.Add(new E4_Users
            {
                id = 3,
                firstName = "Jenny",
                surname = "Singh",
                gender = "Male",
                email = "SJenny@e4.co.za",
                telephone_Number = "9849453963",
                city = "Johannesburg",
                dateofBirth = "291386",
                notes = "This is test Notes3",
            });
        }
    }

    public class E4_Users
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
        public string telephone_Number { get; set; }
        public string city { get; set; }
        public string dateofBirth { get; set; }
        public string notes { get; set; }
    }
}
